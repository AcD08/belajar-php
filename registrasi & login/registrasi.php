<?php 

    require 'functions.php';

    if (isset($_POST["register"])) {
        
        if (regist($_POST) > 0) {
            echo "
            <script>
                alert ('berhasil registrasi!');
            </script>
            ";
        } else {
            echo mysqli_error($DB);
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Registrasi</title>
    <style>
        label {
            display: block;
        }
    </style>
</head>
<body>

<h1>Halaman Registrasi</h1>

<form action="" method="post">

    <ul>
        <li>
        <label for="username">Username: </label>
        <input type="text" name="username" id="username">
        </li>
        <li>
        <label for="Password">Password: </label>
        <input type="password" name="Password" id="Password">
        </li>
        <li>
        <label for="Konfirmasi Password">Konfirmasi Password: </label>
        <input type="password" name="konfirmPassword" id="konfirmPassword">
        </li>
        <button type="submit" name="register">Register!</button>
    </ul>

</form>
    
</body>
</html>