<?php 

// abstract class = sebuah kelas yang tidak dapat di-instansiasi. Digunakan dalam pewarisan untuk memaksakan implementasi method abstrak yang sama untuk semua kelas turunannya

// penggunaan abstrak = merepresentasikan ide 


abstract class Produk {
    private $judul,
           $penulis,
           $penerbit,
           $tahunRilis,
           $harga,
           $diskon = 0;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis ) {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
        $this->tahunRilis = $tahunRilis;
    }

    public function setJudul ($judul) {
        return $this->judul = $judul;
    }

    public function getJudul() {
        return $this->judul;
    }

    public function setPenulis ($penulis) {
        return $this->penulis = $penulis;
    }

    public function getPenulis() {
        return $this->penulis;
    }

    public function setPenerbit ($penerbit) {
        return $this->penerbit = $penerbit;
    }

    public function getPenerbit () {
        return $this->penerbit;
    }

    public function setTahunRilis ($tahunRilis) {
        return $this->tahunRilis = $tahunRilis;
    }

    public function getTahunRilis () {
        return $this->tahunRilis;
    }

    public function setHarga ($harga) {
        return $this->harga = $harga;
    }

    public function getHarga () {
        return $this->harga - ($this->harga * $this->diskon / 100);
    }

    public function setDiskon ($diskon) {
        $this->diskon = $diskon;
    }

    public function getDiskon() {
        return $this->diskon;
    }


    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    } 

    abstract public function getInfoProduk();

    public function getInfo() {
        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

}

class Komik extends Produk {

    public $jmlhHalaman;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $jmlhHalaman) {
        parent::__construct($judul, $penulis, $penerbit, $harga, $tahunRilis);
        $this->jmlhHalaman = $jmlhHalaman;
    }

    public function getInfoProduk()
    {
        $str = "Komik : " . $this->getInfo()  . " - {$this->jmlhHalaman} Halaman.";
        return $str;
    }

}

class Game extends Produk {

    public $waktuMain;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $waktuMain) {
        parent::__construct($judul, $penulis, $penerbit, $harga, $tahunRilis);
        $this->waktuMain = $waktuMain;
    }

    public function getInfoProduk()
    {
        $str = "Game : " . $this->getInfo()  . " ~ {$this->waktuMain} Jam.";
        return $str;
    }
}

class CetakInfoProduk {
    public $daftarProduk = [];

    public function tambahProduk(Produk $produk) {
        $this->daftarProduk[] = $produk; 
    }

    public function cetak() {
        $str = "DAFTAR PRODUK: <br>";

        foreach ($this->daftarProduk as $p) {
            $str .= "- {$p->getInfoProduk()} <br>";
        }
        return $str;
    }
}


$produk1 = new Komik('Naruto', 'Masashi Kishimoto', 'Shonen Jump', 30000, 1995, 40);
$produk2 = new Game('Metal Gear Solid V: The Phantom Pain', 'Kojima Hideo', 'Konami', 450000, 201, 50);

$cetakProduk = new CetakInfoProduk ();
$cetakProduk->tambahProduk($produk1);
$cetakProduk->tambahProduk($produk2);
echo $cetakProduk->cetak();


