<?php 

if (!isset($_GET["nama"]) || 
    !isset($_GET["NIM"]) ||
    !isset($_GET["email"]) ||
    !isset($_GET["jurusna"]) ||
    !isset($_GET["gambar"]) ) {
    header("Location: index.php");
    exit;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Mahasiswa</title>
    <style>
    img {
        width: 200px;
    }
    
    </style>
</head>
<body>
<ul>
    <li><img src="img/<?= $_GET["gambar"];?>"></li>
    <li><?= $_GET["nama"]; ?></li>
    <li><?= $_GET["NIM"]; ?></li>
    <li><?= $_GET["email"]; ?></li>
    <li><?= $_GET["jurusan"]; ?></li>
</ul>

    <a href="index.php">Kembali ke daftar mahasiswa</a>
</body>
</html>