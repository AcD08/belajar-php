<?php

//  namespace: sebuah cara untuk mengelompokkan program ke dalam sebuah package tersendiri


require 'App/init.php';

// $produk1 = new Komik('Naruto', 'Masashi Kishimoto', 'Shonen Jump', 30000, 1995, 40);
// $produk2 = new Game('Metal Gear Solid V: The Phantom Pain', 'Kojima Hideo', 'Konami', 450000, 201, 50);

// $cetakProduk = new CetakInfoProduk ();
// $cetakProduk->tambahProduk($produk1);
// $cetakProduk->tambahProduk($produk2);
// echo $cetakProduk->cetak();


use App\Services\User as ServiceUser; //memberikan alias
use App\Product\User as ProdukUser;

new ServiceUser();