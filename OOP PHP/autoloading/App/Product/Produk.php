<?php 

abstract class Produk {
    protected $judul,
           $penulis,
           $penerbit,
           $tahunRilis,
           $harga,
           $diskon = 0;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis ) {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
        $this->tahunRilis = $tahunRilis;
    }

    public function setJudul ($judul) {
        return $this->judul = $judul;
    }

    public function getJudul() {
        return $this->judul;
    }

    public function setPenulis ($penulis) {
        return $this->penulis = $penulis;
    }

    public function getPenulis() {
        return $this->penulis;
    }

    public function setPenerbit ($penerbit) {
        return $this->penerbit = $penerbit;
    }

    public function getPenerbit () {
        return $this->penerbit;
    }

    public function setTahunRilis ($tahunRilis) {
        return $this->tahunRilis = $tahunRilis;
    }

    public function getTahunRilis () {
        return $this->tahunRilis;
    }

    public function setHarga ($harga) {
        return $this->harga = $harga;
    }

    public function getHarga () {
        return $this->harga - ($this->harga * $this->diskon / 100);
    }

    public function setDiskon ($diskon) {
        $this->diskon = $diskon;
    }

    public function getDiskon() {
        return $this->diskon;
    }


    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    } 

    abstract public function getInfo();

}