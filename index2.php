<?php 

// Standar Output
// echo, print
// print_r
// var_dump

// Keyword Global: untuk mengakses variabel global dari dalam function
// Keyword static: untuk menyimpan local variabel agar tidak terhapus

// Penulisan sintaks PHP
// 1. PHP di dalam HTML
// 2. HTML di dalam PHP

// Variabel dan tipe data
// variabel = dibuat dengan tanda $

// Penggabungan String
// $nama_depan = "Axel";
// $nama_belakang = "Christopher";
// echo $nama_depan . " " . $nama_belakang;

// Assignment
// .=
// $nama = "Sandhika";
// $nama .= " ";
// $nama .= "Galih";
// echo $nama;

// Identitas 
// ===, !===

?>

<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar PHP</title>
</head>
<body>
    <h1>Halo, selamat datang <?php echo 'keren loe' ?> </h1>

</body>
</html> -->
