<?php 

// Properti: mempresentasikan data /  keadaan dari sebuah object
// Method: mempresentasikan perilaku dari sebuah object atau function yang ada di dalam object
// 


class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $harga,
           $tahunRilis;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis) {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
        $this->tahunRilis = $tahunRilis;
    }


    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    } 
}

class CetakInfoProduk {
    public function cetak(Produk $produk) {
        $str = "{$produk->judul} | {$produk->getLabel()} | {$produk->harga} | {$produk->tahunRilis}";
        return $str;
    }
}


$produk1 = new Produk('Naruto', 'Masashi Kishimoto', 'Shonen Jump', 30000, 1995);
$produk2 = new Produk('Metal Gear Solid V: The Phantom Pain', 'Kojima Hideo', 'Konami', 450000, 2015);

echo "Komik : " . $produk1->getLabel();
echo "<br>";
echo "Game : " . $produk2->getLabel();
echo "<br>";
$infoProduk1 = new CetakInfoProduk();
echo $infoProduk1->cetak($produk1);




