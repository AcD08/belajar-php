<?php 

// $mahasiswa = [
//     ["Sandhika Galih", "00404040404", "Teknik Informatika", "Sandhika@gmail.com"],
//     ["Vico Keren", "00202020402", "Teknik Informatika", "vicokeren@gmail.com"],
//     ["Gweh", "00505020402", "Teknik Informatika", "gweh@gmail.com"]
// ];


// Array Assosiative
// Definisinya sama dengan array numerik, kecuali
// Key nya adalah string yang kita buat sendiri
$mahasiswa = [
    [
    "nama" => "Sandhika Galih", 
    "NIM" => "0404040404",
    "email" => "sandhika@gmail.com",
    "jurusan" => "teknik informatika",
    "gambar" => "faiz.jpg"
    ], 
    [
        "nama" => "Galih", 
        "NIM" => "0203060405",
        "email" => "galih@gmail.com",
        "jurusan" => "teknik informatika",
        "tugas" => [90, 80, 70],
        "gambar" => "faiz2.jpg"
     ]

];



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa</title>
    <style>
    img {
        width: 200px;
    }
    </style>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
    <?php foreach ($mahasiswa as $mhs) : ?>
    <ul>
        <li><img src="img/<?= $mhs["gambar"]; ?>" ></li>
        <li>Nama: <?= $mhs["nama"]; ?></li>
        <li>NIM: <?= $mhs["NIM"]; ?></li>
        <li>Jurusan: <?= $mhs["email"]; ?></li>
        <li>Email: <?= $mhs["jurusan"]; ?></li>
    </ul>
    <?php endforeach; ?>
</body>
</html>