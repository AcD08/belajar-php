<?php

//  autoloading: memanggil class (file) tanpa harus menggunakan require

require 'App/init.php';

$produk1 = new Komik('Naruto', 'Masashi Kishimoto', 'Shonen Jump', 30000, 1995, 40);
$produk2 = new Game('Metal Gear Solid V: The Phantom Pain', 'Kojima Hideo', 'Konami', 450000, 201, 50);

$cetakProduk = new CetakInfoProduk ();
$cetakProduk->tambahProduk($produk1);
$cetakProduk->tambahProduk($produk2);
echo $cetakProduk->cetak();